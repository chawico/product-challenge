package com.chawico.product.entrypoint;

import static com.chawico.product.utils.RestUtils.toJson;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import com.chawico.product.entrypoint.advice.ErrorHandlerControllerAdvice;
import com.chawico.product.entrypoint.model.in.ProductModelIn;
import com.chawico.product.usecase.ProductUseCase;

@ExtendWith(SpringExtension.class)
@WebMvcTest
@ContextConfiguration(classes = {ProductController.class, ErrorHandlerControllerAdvice.class})
public class ProductController4xxTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private ProductUseCase productUseCase;
    
	private static final String GENERIC_ERROR_MESSAGE = "Há um ou mais erros nas informações enviadas.";
    
	@DisplayName("Ao incluir um produto sem fornecer o mínimo de dados o retorno deve ser um status HTTP 400")
	@Test
	public void incluirProdutoBadRequest() throws Exception {
		ProductModelIn product = new ProductModelIn(null, null, null);
		
		this.mvc.perform(post("/products")
				.content(toJson(product))
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isBadRequest())
				.andExpect(jsonPath("$.status_code", is(equalTo(HttpStatus.BAD_REQUEST.value()))))
				.andExpect(jsonPath("$.message", is(containsString(GENERIC_ERROR_MESSAGE))));
	}
	
	@DisplayName("Ao atualizar um produto sem fornecer o mínimo de dados o retorno deve ser um status HTTP 400")
	@Test
	public void atualizarProdutoBadRequest() throws Exception {
		String id = "bad_request";
		ProductModelIn product = new ProductModelIn(null, null, null);
		
		this.mvc.perform(put("/products/{id}", id)
				.content(toJson(product))
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isBadRequest())
				.andExpect(jsonPath("$.status_code", is(equalTo(HttpStatus.BAD_REQUEST.value()))))
				.andExpect(jsonPath("$.message", is(containsString(GENERIC_ERROR_MESSAGE))));
	}
	
	@DisplayName("Ao incluir um produto criando uma requisição má formada deve retornar a estrutura de erro com campos status_code e message")
	@Test
	public void requisicaoIncorreta() throws Exception {
		ProductModelIn product = new ProductModelIn(null, null, null);
		
		this.mvc.perform(post("/products")
				.content(toJson(product)))
				.andExpect(status().is4xxClientError())
				.andExpect(jsonPath("$.message", is(containsString(GENERIC_ERROR_MESSAGE))));
	}
}
