package com.chawico.product.entrypoint;

import static com.chawico.product.utils.RestUtils.toJson;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.math.BigDecimal;
import java.util.Optional;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import com.chawico.product.entrypoint.model.in.ProductModelIn;
import com.chawico.product.usecase.ProductUseCase;
import com.chawico.product.usecase.gateway.ProductGateway;

@ExtendWith(SpringExtension.class)
@WebMvcTest
@ContextConfiguration(classes = {ProductController.class, ProductUseCase.class})
public class ProductControllerWithUCTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private ProductGateway productGateway;
    
	@DisplayName("Ao atualizar um produto informando um ID que não existe deve retornar um status HTTP 404")
	@Test
	public void atualizarProdutoInexistente() throws Exception {
		String id = "not_found";
		ProductModelIn product = new ProductModelIn("bicicleta", "18 marchas", BigDecimal.valueOf(2400.30));
		
		given(this.productGateway.findById(id)).willReturn(Optional.empty());
		
		this.mvc.perform(put("/products/{id}", id)
				.content(toJson(product))
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isNotFound());
	}
	
	@DisplayName("Ao remover um produto informando um ID que não existe deve retornar um status HTTP 404")
	@Test
	public void removerProdutoInexistente() throws Exception {
		String id = "not_found_delete";
		
		given(this.productGateway.findById(id)).willReturn(Optional.empty());
		
		this.mvc.perform(delete("/products/{id}", id)
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isNotFound());
	}		
	
}
