package com.chawico.product.entrypoint;

import static com.chawico.product.utils.RestUtils.toJson;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.number.IsCloseTo.closeTo;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import com.chawico.product.entrypoint.model.in.ProductModelIn;
import com.chawico.product.usecase.ProductUseCase;
import com.chawico.product.usecase.domain.ProductDomain;

@ExtendWith(SpringExtension.class)
@WebMvcTest
@ContextConfiguration(classes = ProductController.class)
public class ProductController2xxTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private ProductUseCase productUseCase;
    
    private static final double ERROR = 0.01;
    
	@DisplayName("Inclusão de um produto com sucesso")
	@Test
	public void incluirProduto() throws Exception {
		ProductModelIn productModel = getProductModelMock();
		ProductDomain productDomain = convertToProductDomain(null, productModel);
		
		given(productUseCase.criar(any(ProductDomain.class))).willReturn(productDomain);
		
		this.mvc.perform(post("/products")
				.content(toJson(productModel))
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isCreated())
				.andExpect(jsonPath("$.id", is(productDomain.getId())))
				.andExpect(jsonPath("$.name", is(equalTo(productDomain.getName()))))
				.andExpect(jsonPath("$.description", is(equalTo(productDomain.getDescription()))))
				.andExpect(jsonPath("$.price", is(closeTo(productDomain.getPrice().doubleValue(), ERROR))));
	}
	
	@DisplayName("Atualização de um produto com sucesso")
	@Test
	public void atualizarProduto() throws Exception {
		String id = "#123";
		
		ProductModelIn productModel = getProductModelMock();
		ProductDomain productDomain = convertToProductDomain(id, productModel);
		
		given(productUseCase.update(any(ProductDomain.class))).willReturn(Optional.of(productDomain));
		
		this.mvc.perform(put("/products/{id}", id)
				.content(toJson(productModel))
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.id", is(productDomain.getId())))
				.andExpect(jsonPath("$.name", is(equalTo(productDomain.getName()))))
				.andExpect(jsonPath("$.description", is(equalTo(productDomain.getDescription()))))
				.andExpect(jsonPath("$.price", is(closeTo(productDomain.getPrice().doubleValue(), ERROR))));
	}
	
	@DisplayName("Remoção de um produto com sucesso")
	@Test
	public void removerProduto() throws Exception {
		String id = "#remover";
		given(productUseCase.delete(id)).willReturn(Boolean.TRUE);
		
		this.mvc.perform(delete("/products/{id}", id)
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk());
	}	

	@DisplayName("Consulta de um produto com sucesso")
	@Test
	public void consultarProduto() throws Exception {
		String id = "#consulta";
		
		ProductModelIn productModel = getProductModelMock();
		ProductDomain productDomain = convertToProductDomain(id, productModel);
		
		given(productUseCase.findById(id)).willReturn(Optional.of(productDomain));
		
		this.mvc.perform(get("/products/{id}", id)
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.id", is(productDomain.getId())))
				.andExpect(jsonPath("$.name", is(equalTo(productDomain.getName()))))
				.andExpect(jsonPath("$.description", is(equalTo(productDomain.getDescription()))))
				.andExpect(jsonPath("$.price", is(closeTo(productDomain.getPrice().doubleValue(), ERROR))));
	}		
    
	@DisplayName("Consulta todos os produtos cadastrados")
	@Test
	public void consultarTodosProdutosComRegistros() throws Exception {
		
		List<ProductDomain> products = new ArrayList<>();
		ProductDomain product = ProductDomain.builder()
				.id("id1")
				.name("Produto 1")
				.description("Desc 1")
				.price(BigDecimal.TEN)
				.build();
		products.add(product);
		
		product = ProductDomain.builder()
				.id("id2")
				.name("Produto 2")
				.description("Desc 2")
				.price(BigDecimal.ONE)
				.build();
		products.add(product);
		
		given(productUseCase.findAll()).willReturn(products);
		
		this.mvc.perform(get("/products"))
			.andExpect(status().isOk())
			.andExpect(content().contentType(MediaType.APPLICATION_JSON))
			.andExpect(jsonPath("$.length()", is(2)))
			.andExpect(jsonPath("$[0].id", is("id1")))
			.andExpect(jsonPath("$[1].id", is("id2")));
	}
	
	private ProductModelIn getProductModelMock() {
		return new ProductModelIn("Engrenagem", "ré", BigDecimal.valueOf(0.55));
	}
	
	private ProductDomain convertToProductDomain(String id, ProductModelIn productModel) {
		return ProductDomain.builder()
				.id(id == null ? UUID.randomUUID().toString() : id)
				.name(productModel.getName())
				.description(productModel.getDescription())
				.price(productModel.getPrice())
				.build();
	}
}
