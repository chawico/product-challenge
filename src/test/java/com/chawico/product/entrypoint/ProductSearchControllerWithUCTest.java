package com.chawico.product.entrypoint;

import static org.hamcrest.collection.IsEmptyCollection.empty;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import com.chawico.product.usecase.ProductSearchUseCase;
import com.chawico.product.usecase.domain.ProductSearchDomain;
import com.chawico.product.usecase.gateway.ProductSearchGateway;

@ExtendWith(SpringExtension.class)
@WebMvcTest
@ContextConfiguration(classes = {ProductSearchController.class, ProductSearchUseCase.class})
public class ProductSearchControllerWithUCTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private ProductSearchGateway productSearchGateway;
    
	@DisplayName("Ao consultar Produtos e de acordo com os critérios informados não encontrar resultado então deve retornar um status HTTP 200 e um array vazio")
	@Test
	public void consultarProdutosSemResultado() throws Exception {
		MultiValueMap<String, String> queryParams = new LinkedMultiValueMap<>();
		queryParams.put("q", Arrays.asList("inexistente"));
		
		given(this.productSearchGateway.search(any(ProductSearchDomain.class))).willReturn(new ArrayList<>());
		
		this.mvc.perform(get("/products/search")
				.queryParams(queryParams)
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$", empty()));
	}
}
