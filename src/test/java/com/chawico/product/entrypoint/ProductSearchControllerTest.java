package com.chawico.product.entrypoint;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import com.chawico.product.usecase.ProductSearchUseCase;
import com.chawico.product.usecase.domain.ProductDomain;
import com.chawico.product.usecase.domain.ProductSearchDomain;

@ExtendWith(SpringExtension.class)
@WebMvcTest
@ContextConfiguration(classes = {ProductSearchController.class})
public class ProductSearchControllerTest {

    @Autowired
    private MockMvc mvc;
    
    @MockBean
    private ProductSearchUseCase productSearchUseCase;
    
	@DisplayName("Ao consultar Produtos e de acordo com os critérios informados encontrar resultados então deve retornar um status HTTP 200 e os registros encontrados")
	@Test
	public void consultarProdutosCriterioBuscaComRegistros() throws Exception {
		MultiValueMap<String, String> queryParams = new LinkedMultiValueMap<>();
		queryParams.put("q", Arrays.asList("prod"));
		queryParams.put("min_price", Arrays.asList(BigDecimal.ONE.toString()));
		queryParams.put("max_price", Arrays.asList(BigDecimal.TEN.toString()));
		
		List<ProductDomain> products = new ArrayList<>();
		products.add(this.addProduct("2", "Produto 2", "kit", BigDecimal.valueOf(2.25)));
		products.add(this.addProduct("5", "Barraca", "produto de lona", BigDecimal.valueOf(9.25)));

		given(this.productSearchUseCase.search(any(ProductSearchDomain.class))).willReturn(products);
		
		this.mvc.perform(get("/products/search").queryParams(queryParams))
			.andExpect(status().isOk())
			.andExpect(content().contentType(MediaType.APPLICATION_JSON))
			.andExpect(jsonPath("$.length()", is(2)))
			.andExpect(jsonPath("$[0].id", is("2")))
			.andExpect(jsonPath("$[1].id", is("5")));
	}
	
	private ProductDomain addProduct(String id, String name, String description, BigDecimal price) {
		return ProductDomain.builder()
				.id(id)
				.name(name)
				.description(description)
				.price(price)
				.build();
	}
	
}
