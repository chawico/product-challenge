package com.chawico.product.usecase;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.chawico.product.dataprovider.ProductDataProvider;
import com.chawico.product.dataprovider.entity.ProductEntity;
import com.chawico.product.dataprovider.repository.ProductRepository;
import com.chawico.product.usecase.domain.ProductDomain;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {ProductUseCase.class, ProductDataProvider.class})
public class ProductUseCaseWithDPTest {

	@Autowired
	private ProductUseCase productUseCase;
	
	@MockBean
	private ProductRepository productRepository;
	
	@DisplayName("Insere um produto que deve retornar o ID gerado além dos dados inseridos")
	@Test
	public void inserirProduto() throws Exception {
		ProductDomain productDomain = ProductDomain.builder()
				.name("Name")
				.description("test")
				.price(BigDecimal.ONE)
				.build();
		
		given(this.productRepository.save(any())).will(new Answer<ProductEntity>() {
			@Override
			public ProductEntity answer(InvocationOnMock invocation) throws Throwable {
				return invocation.getArgument(0);
			}
		});
		
		ProductDomain createdProduct = this.productUseCase.criar(productDomain);
		assertNotNull(createdProduct.getId());
	}
	
	@DisplayName("Consulta todos produtos")
	@Test
	public void consultaProdutos() throws Exception {
		String id = "1212";
		ProductEntity productEntity = ProductEntity.builder()
				.id(id)
				.name("Product")
				.description("junit")
				.price(BigDecimal.TEN)
				.build();
		List<ProductEntity> products = new ArrayList<>();
		products.add(productEntity);
		
		given(this.productRepository.findAll()).willReturn(products);
		
		List<ProductDomain> createdProducts = this.productUseCase.findAll();
		assertEquals(1, createdProducts.size());
		assertEquals(id, createdProducts.get(0).getId());
	}
}
