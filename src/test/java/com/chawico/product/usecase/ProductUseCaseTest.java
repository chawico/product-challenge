package com.chawico.product.usecase;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.math.BigDecimal;
import java.util.Optional;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.chawico.product.usecase.domain.ProductDomain;
import com.chawico.product.usecase.gateway.ProductGateway;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {ProductUseCase.class})
public class ProductUseCaseTest {

	@Autowired
	private ProductUseCase productUseCase;
	
	@MockBean
	private ProductGateway productGateway;
	
	@DisplayName("Atualza um produto existente")
	@Test
	public void atualizarProdutoExistente() throws Exception {
		String id = "update";

		ProductDomain productDomainOld = ProductDomain.builder()
				.id(id)
				.name("Old")
				.description("older")
				.price(BigDecimal.ONE)
				.build();
		ProductDomain productDomain = ProductDomain.builder()
				.id(id)
				.name("Update")
				.description("updated")
				.price(BigDecimal.TEN)
				.build();
		
		given(this.productGateway.findById(id)).willReturn(Optional.of(productDomainOld));
		given(this.productGateway.update(any())).will(new Answer<ProductDomain>() {
			@Override
			public ProductDomain answer(InvocationOnMock invocation) throws Throwable {
				return invocation.getArgument(0);
			}
		});
		
		Optional<ProductDomain> updatedProduct = this.productUseCase.update(productDomain);
		verify(this.productGateway, times(1)).update(productDomain);
		
		assertEquals("Update", updatedProduct.get().getName());
	}

	
	@DisplayName("Remove um produto existente")
	@Test
	public void removerProdutoExistente() throws Exception {
		String id = "delete";

		ProductDomain productDomainOld = ProductDomain.builder()
				.id(id)
				.name("Actual")
				.description("ok")
				.price(BigDecimal.ONE)
				.build();
		
		given(this.productGateway.findById(id)).willReturn(Optional.of(productDomainOld));
		
		Boolean result = this.productUseCase.delete(id);
		verify(this.productGateway, times(1)).delete(id);
		
		assertTrue(result);
	}
}
