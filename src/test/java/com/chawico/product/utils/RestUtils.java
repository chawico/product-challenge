package com.chawico.product.utils;

import com.fasterxml.jackson.databind.ObjectMapper;

public class RestUtils {

	private static final ObjectMapper OBJECT_MAPPER;
	
	static {
		OBJECT_MAPPER = new ObjectMapper();
	}
	
	/**
	 * Construtor privado para evitar instanciação.
	 */
	private RestUtils() {
		
	}
	
	public static String toJson(Object data) throws Exception {
		return OBJECT_MAPPER.writeValueAsString(data);
	}	
}
