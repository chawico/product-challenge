package com.chawico.product.dataprovider;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.math.BigDecimal;
import java.util.List;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ContextConfiguration;

import com.chawico.product.dataprovider.entity.ProductEntity;
import com.chawico.product.dataprovider.repository.ProductRepository;
import com.chawico.product.usecase.domain.ProductDomain;
import com.chawico.product.usecase.domain.ProductSearchDomain;

@ContextConfiguration(classes = {ProductSearchDataProvider.class})
@DataJpaTest
@EnableAutoConfiguration
public class ProductSearchDataProviderTest {

    @Autowired
    private ProductSearchDataProvider productSearchDataProvider;

    @Autowired
    private ProductRepository productRepository;
    
    @BeforeEach
    public void setup() {
    	insertProduct("001", "Produto 001", "recomendado", BigDecimal.valueOf(0.01));
    	insertProduct("100", "Produto 100", "recomendado", BigDecimal.valueOf(1.00));
    	insertProduct("200", "223040", "melhor produto", BigDecimal.valueOf(10.00));
    	insertProduct("300", "Fora", "range", BigDecimal.valueOf(10.01));
    }
    
    @AfterEach
    public void tearDown() {
    	this.productRepository.deleteAll();
    }
    
	@DisplayName("Consulta todos os produtos cadastrados conforme critérios de busca informados")
	@Test
	public void consultarProdutosCriterioBuscaComRegistros() throws Exception {
		ProductSearchDomain productSearch = ProductSearchDomain.builder()
				.term("prod")
				.minPrice(BigDecimal.ONE)
				.maxPrice(BigDecimal.TEN)
				.build();

		List<ProductDomain> searchResult = this.productSearchDataProvider.search(productSearch);
		assertEquals(2, searchResult.size());
		
		long items = searchResult.stream()
			.filter(product -> "100".equals(product.getId()) || "200".equals(product.getId()))
			.count();
		assertEquals(2, items);
		
	}
	
	
	@DisplayName("Consulta todos os produtos cadastrados apenas pelo preço máximo")
	@Test
	public void consultarProdutosPrecoMaximoComRegistros() throws Exception {
		ProductSearchDomain productSearch = ProductSearchDomain.builder()
				.maxPrice(BigDecimal.ONE)
				.build();

		List<ProductDomain> searchResult = this.productSearchDataProvider.search(productSearch);
		assertEquals(2, searchResult.size());
		
		long items = searchResult.stream()
			.filter(product -> "001".equals(product.getId()) || "100".equals(product.getId()))
			.count();
		assertEquals(2, items);
	}
	
	private void insertProduct(String id, String name, String description, BigDecimal price) {
		ProductEntity entity = ProductEntity.builder()
				.id(id)
				.name(name)
				.description(description)
				.price(price)
				.build();
		this.productRepository.save(entity);
	}
	
}
