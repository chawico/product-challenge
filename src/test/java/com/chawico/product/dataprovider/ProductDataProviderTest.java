package com.chawico.product.dataprovider;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

import java.math.BigDecimal;
import java.util.Optional;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ContextConfiguration;

import com.chawico.product.dataprovider.entity.ProductEntity;
import com.chawico.product.dataprovider.repository.ProductRepository;
import com.chawico.product.usecase.domain.ProductDomain;

@ContextConfiguration(classes = { ProductDataProvider.class })
@DataJpaTest
@EnableAutoConfiguration
public class ProductDataProviderTest {

	@Autowired
	private ProductDataProvider productDataProvider;

	@Autowired
	private ProductRepository productRepository;

    @AfterEach
    public void tearDown() {
    	this.productRepository.deleteAll();
    }

	@DisplayName("Consulta produto específico")
	@Test
	public void consultaProduto() {
		String id = "123";
		this.insertProduct(id, "N", "D", BigDecimal.ONE);
		
		Optional<ProductDomain> product = this.productDataProvider.findById(id);
		assertEquals(id, product.get().getId());
	}
    
	@DisplayName("Atualiza um produto específico")
	@Test
	public void atualizaProduto() {
		String id = "456";
		this.insertProduct(id, "A", "B", BigDecimal.ONE);
		
		ProductDomain newProduct = ProductDomain.builder()
				.id(id)
				.name("C")
				.description("D")
				.price(BigDecimal.TEN)
				.build();
		ProductDomain updatedProduct = this.productDataProvider.update(newProduct);
		assertEquals(newProduct.getName(), updatedProduct.getName());
		assertEquals(newProduct.getDescription(), updatedProduct.getDescription());
		assertEquals(newProduct.getPrice(), updatedProduct.getPrice());
	}
	
	@DisplayName("Remove um produto específico")
	@Test
	public void removeProduto() {
		String id = "999";
		this.insertProduct(id, "A", "B", BigDecimal.ONE);
		
		this.productDataProvider.delete(id);
		
		Optional<ProductDomain> product = this.productDataProvider.findById(id);
		assertFalse(product.isPresent());
	}
	
	private void insertProduct(String id, String name, String description, BigDecimal price) {
		ProductEntity entity = ProductEntity.builder()
				.id(id)
				.name(name)
				.description(description)
				.price(price)
				.build();
		this.productRepository.save(entity);
	}
}
