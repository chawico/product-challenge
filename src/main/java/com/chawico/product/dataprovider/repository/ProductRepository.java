package com.chawico.product.dataprovider.repository;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.chawico.product.dataprovider.entity.ProductEntity;

/**
 * Componente responsável pela interação com o banco de dados no que se refere a entidade de Produtos.
 *  
 * @author chawico
 * @since 12/04/2021
 */
@Repository
public interface ProductRepository extends CrudRepository<ProductEntity, String>, JpaSpecificationExecutor<ProductEntity> {
}
