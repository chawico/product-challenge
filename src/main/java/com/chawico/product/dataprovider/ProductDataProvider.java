package com.chawico.product.dataprovider;

import static com.chawico.product.dataprovider.entity.ProductEntityMapper.toDomain;
import static com.chawico.product.dataprovider.entity.ProductEntityMapper.toEntity;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.chawico.product.dataprovider.entity.ProductEntity;
import com.chawico.product.dataprovider.repository.ProductRepository;
import com.chawico.product.usecase.domain.ProductDomain;
import com.chawico.product.usecase.gateway.ProductGateway;

/**
 * Provedor de dados para realização de consultas, inserção, deleção e atualização de produtos.
 *  
 * @author chawico
 * @since 12/04/2021
 */
@Component
public class ProductDataProvider implements ProductGateway {

	@Autowired
	private ProductRepository productRepository;

	@Override
	public ProductDomain create(ProductDomain productDomain) {
		String id = UUID.randomUUID().toString();
		ProductEntity productData = toEntity(id, productDomain);

		ProductEntity productEntity = this.productRepository.save(productData);
		return toDomain(productEntity);
	}

	@Override
	public ProductDomain update(ProductDomain productDomain) {
		ProductEntity productData = toEntity(null, productDomain);

		ProductEntity productEntity = this.productRepository.save(productData);
		return toDomain(productEntity);
	}
	
	@Override
	public void delete(String id) {
		this.productRepository.deleteById(id);
	}

	@Override
	public Optional<ProductDomain> findById(String id) {
		return toDomain(this.productRepository.findById(id));
	}
	
	@Override
	public List<ProductDomain> findAll() {
		Iterable<ProductEntity> products = this.productRepository.findAll();
		return toDomain(products);
	}
}
