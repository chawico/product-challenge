package com.chawico.product.dataprovider.spec;

import java.math.BigDecimal;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.util.ObjectUtils;

import com.chawico.product.dataprovider.entity.ProductEntity;

/**
 * Construtor de {@code Specification} para realizar consultas avançadas na entidade de Produto
 * 
 * @author chawico
 * @since 12/04/2021
 */
public class ProductSearchSpecs {

	/**
	 * Construtor privado para evitar instanciação.
	 */
	private ProductSearchSpecs() {
		
	}
	
	private static Predicate doLike(Root<ProductEntity> root, CriteriaBuilder builder, String fieldName, String term) {
		if (ObjectUtils.isEmpty(term)) {
			return builder.conjunction();
		}

		//Lower case para a busca ser case insensitive
		String searchTerm = "%" + term + "%";
		searchTerm = searchTerm.toLowerCase();
		return builder.like(builder.lower(root.get(fieldName)), searchTerm);
	}
	
	/**
	 * Aplica filtro like no campo name.
	 * 
	 * @param term {@code String} - termo a ser buscado
	 * @return Specification que filtra o campo name
	 */
	public static Specification<ProductEntity> hasName(String term) {
		return (root, query, builder) -> {
			return doLike(root, builder, "name", term);
		};
	}
	
	/**
	 * Aplica filtro like no campo description.
	 * 
	 * @param term {@code String} - termo a ser buscado
	 * @return Specification que filtra o campo description
	 */
	public static Specification<ProductEntity> hasDescription(String term) {
		return (root, query, builder) -> {
			return doLike(root, builder, "description", term);
		};
	}

	/**
	 * Aplica filtro maior ou igual a no campo price.
	 * 
	 * @param price {@code BigDecimal} - preço a ser considerado para filtro
	 * @return Specification que filtra o campo price
	 */
	public static Specification<ProductEntity> hasPriceEqualOrGreaterThan(BigDecimal price) {
		return (root, query, builder) -> {
			if (price == null) {
				return builder.conjunction();
			}
			return builder.greaterThanOrEqualTo(root.get("price"), price);
		};
	}

	/**
	 * Aplica filtro menor ou igual a no campo price.
	 * 
	 * @param price {@code BigDecimal} - preço a ser considerado para filtro
	 * @return Specification que filtra o campo price
	 */	
	public static Specification<ProductEntity> hasPriceEqualOrLessThan(BigDecimal price) {
		return (root, query, builder) -> {
			if (price == null) {
				return builder.conjunction();
			}
			return builder.lessThanOrEqualTo(root.get("price"), price);
		};
	}
}
