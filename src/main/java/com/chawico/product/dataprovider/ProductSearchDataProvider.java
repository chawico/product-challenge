package com.chawico.product.dataprovider;

import static com.chawico.product.dataprovider.entity.ProductEntityMapper.toDomain;
import static com.chawico.product.dataprovider.spec.ProductSearchSpecs.hasDescription;
import static com.chawico.product.dataprovider.spec.ProductSearchSpecs.hasName;
import static com.chawico.product.dataprovider.spec.ProductSearchSpecs.hasPriceEqualOrGreaterThan;
import static com.chawico.product.dataprovider.spec.ProductSearchSpecs.hasPriceEqualOrLessThan;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;

import com.chawico.product.dataprovider.entity.ProductEntity;
import com.chawico.product.dataprovider.repository.ProductRepository;
import com.chawico.product.usecase.domain.ProductDomain;
import com.chawico.product.usecase.domain.ProductSearchDomain;
import com.chawico.product.usecase.gateway.ProductSearchGateway;

/**
 * Provedor de dados para realização de consultas avançadas de Produtos.
 *  
 * @author chawico
 * @since 12/04/2021
 */
@Component
public class ProductSearchDataProvider implements ProductSearchGateway {

	@Autowired
	private ProductRepository productRepository;
	
	@Override
	public List<ProductDomain> search(ProductSearchDomain productSearch) {
		String term = productSearch.getTerm();
		Specification<ProductEntity> specs = 
				hasName(term)
				.or(hasDescription(term))
				.and(hasPriceEqualOrGreaterThan(productSearch.getMinPrice())
				.and(hasPriceEqualOrLessThan(productSearch.getMaxPrice())));
		
		Iterable<ProductEntity> products = this.productRepository.findAll(specs);
		return toDomain(products);
	}
}
