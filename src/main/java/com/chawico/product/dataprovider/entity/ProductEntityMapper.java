package com.chawico.product.dataprovider.entity;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import com.chawico.product.usecase.domain.ProductDomain;

/**
 * Mapper para conversão entre objetos entity e domain.
 * 
 * @author chawico
 * @since 12/04/2021
 */
public class ProductEntityMapper {

	/**
	 * Construtor privado para evitar instanciação.
	 */
	private ProductEntityMapper() {
		
	}
	
	public static ProductEntity toEntity(String id, ProductDomain productDomain) {
		return ProductEntity.builder()
				.id(id != null ? id : productDomain.getId())
				.name(productDomain.getName())
				.description(productDomain.getDescription())
				.price(productDomain.getPrice())
				.build();
	}	
	
	public static ProductDomain toDomain(ProductEntity productEntity) {
		return ProductDomain.builder()
				.id(productEntity.getId())
				.name(productEntity.getName())
				.description(productEntity.getDescription())
				.price(productEntity.getPrice())
				.build();
	}
	
	public static Optional<ProductDomain> toDomain(Optional<ProductEntity> productEntity) {
		ProductDomain result = null;
		if (productEntity.isPresent()) {
			result = toDomain(productEntity.get());
		}
		return Optional.ofNullable(result);
	}	
	
	public static List<ProductDomain> toDomain(Iterable<ProductEntity> products) {
		return StreamSupport.stream(products.spliterator(), true)
				.map(productEntity -> toDomain(productEntity))
				.collect(Collectors.toList());
	}	
}
