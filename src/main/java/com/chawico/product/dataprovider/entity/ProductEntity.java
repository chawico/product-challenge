package com.chawico.product.dataprovider.entity;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * Representação da entidade do banco de dados Produto 
 *  
 * @author chawico
 * @since 12/04/2021
 */
@Entity
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ProductEntity {

	@Id
	private String id;
	private String name;
	private String description;
	private BigDecimal price;
}
