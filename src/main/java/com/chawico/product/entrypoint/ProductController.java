package com.chawico.product.entrypoint;

import static com.chawico.product.entrypoint.model.ProductModelMapper.toModelOut;
import static com.chawico.product.entrypoint.model.ProductModelMapper.toProductDomain;

import java.net.URI;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.chawico.product.entrypoint.model.in.ProductModelIn;
import com.chawico.product.entrypoint.model.out.ProductModelOut;
import com.chawico.product.usecase.ProductUseCase;
import com.chawico.product.usecase.domain.ProductDomain;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * Controller responsável por expor APIs para cadastramento, alteração, remoção e consulta de Produtos. 
 * 
 * @author chawico
 * @since 12/04/2021
 */
@RestController
@Api(value = "Operações referentes ao CRUD de produtos")
@RequestMapping(path = "/products", produces = MediaType.APPLICATION_JSON_VALUE)
public class ProductController {

	@Autowired
	private ProductUseCase productUseCase;
	
	/**
	 * Cria um produto.
	 * 
	 * @param productRequest {@code ProductModelIn} - dados referentes a um Produto
	 * @return dados do produto cadastrado com retorno HTTP 201
	 */
	@ApiOperation("Insere um produto")
	@PostMapping
	public ResponseEntity<ProductModelOut> create(@Valid @RequestBody ProductModelIn productRequest) {
		ProductDomain productDomainRequest = toProductDomain(null, productRequest);
		ProductDomain productDomainResponse = this.productUseCase.criar(productDomainRequest);
		ProductModelOut response = toModelOut(productDomainResponse);
		
        URI location = URI.create(String.format("/products/%s", response.getId()));
        return ResponseEntity.created(location).body(response);
	}
	
	/**
	 * Atualiza um produto conforme identificador.
	 * 
	 * @param id {@code String} - identificador do Produto
	 * @param productRequest {@code ProductModelIn} - dados referentes a um Produto
	 * @return dados do produto atualizado com retorno HTTP 200 ou 404 caso o produto informado não exista
	 */
	@ApiOperation("Atualiza um produto")
	@PutMapping("{id}")
	public ResponseEntity<ProductModelOut> update(@PathVariable(value = "id") String id, @Valid @RequestBody ProductModelIn productRequest) {
		ProductDomain productDomainRequest = toProductDomain(id, productRequest);
		Optional<ProductDomain> productDomainResponse = this.productUseCase.update(productDomainRequest);
		Optional<ProductModelOut> response = toModelOut(productDomainResponse);
		
		return ResponseEntity.of(response);
	}
	
	/**
	 * Remove um produto conforme identificador.
	 * 
	 * @param id {@code String} - identificador do Produto
	 * @return retorno HTTP 200 em caso de sucesso ou 404 caso o produto informado não exista
	 */
	@ApiOperation(value = "Remove um produto")
	@DeleteMapping("{id}")
	public ResponseEntity<?> delete(@PathVariable(value = "id") String id) {
		Boolean deleted = this.productUseCase.delete(id);
		
		ResponseEntity<?> response;
		if (deleted) {
			response = ResponseEntity.ok().build();
		} else {
			response = ResponseEntity.notFound().build();
		}
		return response;
	}
	
	/**
	 * Consulta um produto específico.
	 * 
	 * @param id {@code String} - identificador do Produto
	 * @return dados do produto consultado com retorno HTTP 200 ou 404 caso o produto informado não exista
	 */
	@ApiOperation("Consulta um produto")
	@GetMapping("{id}")
	public ResponseEntity<ProductModelOut> findById(@PathVariable(value = "id") String id) {
		Optional<ProductDomain> productDomainResponse = this.productUseCase.findById(id);
		Optional<ProductModelOut> response = toModelOut(productDomainResponse);

		return ResponseEntity.of(response);
	}
	
	/**
	 * Recupera todos os produtos cadastrados.
	 * 
	 * @return dados dos produtos cadastrados
	 */	
	@ApiOperation("Recupera todos os produtos cadastrados")
	@GetMapping
	public ResponseEntity<List<ProductModelOut>> findAll() {
		List<ProductDomain> productDomainResponse = this.productUseCase.findAll();
		List<ProductModelOut> productsFound = productDomainResponse.parallelStream()
			.map(product -> toModelOut(product))
			.collect(Collectors.toList());
		
		return ResponseEntity.ok(productsFound);
	}
}
