package com.chawico.product.entrypoint;

import static com.chawico.product.entrypoint.model.ProductModelMapper.toModelOut;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.chawico.product.entrypoint.model.in.ProductSearchModelIn;
import com.chawico.product.entrypoint.model.out.ProductModelOut;
import com.chawico.product.usecase.ProductSearchUseCase;
import com.chawico.product.usecase.domain.ProductDomain;
import com.chawico.product.usecase.domain.ProductSearchDomain;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * Controller responsável por realizar consultas avançadas referentes a um Produto 
 * 
 * @author chawico
 * @since 12/04/2021
 */
@RestController
@Api(value = "Operações referentes ao consultas avançadas de produtos")
@RequestMapping(path = "/products/search", produces = MediaType.APPLICATION_JSON_VALUE)
public class ProductSearchController {

	@Autowired
	private ProductSearchUseCase productSearchUseCase;
	
	private ProductSearchDomain toProductSearchDomain(ProductSearchModelIn productSearch) {
		return ProductSearchDomain.builder()
				.term(productSearch.getTerm())
				.minPrice(productSearch.getMinPrice())
				.maxPrice(productSearch.getMaxPrice())
				.build();
	}	
	
	/**
	 * Recupera os produtos cadastrados conforme critérios informados. 
	 * 
	 * @param {@code ProductSearchModelIn} - critérios para busca de produtos
	 * @return dados dos produtos encontrados
	 */	
	@ApiOperation("Recupera os produtos cadastrados conforme critérios informados")
	@GetMapping
	public ResponseEntity<List<ProductModelOut>> search(ProductSearchModelIn productSearch) {
		ProductSearchDomain productSearchDomain = toProductSearchDomain(productSearch);
		List<ProductDomain> productDomainResponse = this.productSearchUseCase.search(productSearchDomain);

		List<ProductModelOut> productsFound = productDomainResponse.parallelStream()
				.map(product -> toModelOut(product))
				.collect(Collectors.toList());		
		return ResponseEntity.ok(productsFound);
	}
	
}
