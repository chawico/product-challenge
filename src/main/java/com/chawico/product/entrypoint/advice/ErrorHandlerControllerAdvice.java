package com.chawico.product.entrypoint.advice;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.chawico.product.entrypoint.model.out.ErrorModelOut;

/**
 * Controller Advice para interceptar exceções e dar o devido tratamento
 *  
 * @author chawico
 * @since 12/04/2021
 */
@ControllerAdvice
public class ErrorHandlerControllerAdvice extends ResponseEntityExceptionHandler {

	@Autowired
	private MessageSource messageSource;
	
	private static final String GENERIC_ERROR_MESSAGE = "Há um ou mais erros nas informações enviadas.";
	private static final String DETAILS = " Detalhes: ";
	
	private String getMessageFromException(MethodArgumentNotValidException exception) {
		StringBuilder builder = new StringBuilder();
		
		Locale locale = Locale.getDefault();
		for (ObjectError error: exception.getAllErrors()) {
			if (builder.length() == 0) {
				builder.append(DETAILS);
			} else {
				builder.append(" // ");
			}
			
			builder.append(messageSource.getMessage(error, locale));
		}
		return builder.toString();
	}
	
	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		
		
		StringBuilder errorMessage = new StringBuilder(GENERIC_ERROR_MESSAGE);
		errorMessage.append(getMessageFromException(ex));
		
		ErrorModelOut error = ErrorModelOut.builder()
			.statusCode(HttpStatus.BAD_REQUEST.value())
			.message(errorMessage.toString())
			.build();
		
		return ResponseEntity.badRequest().body(error);
	}
	
	@Override
	protected ResponseEntity<Object> handleExceptionInternal(Exception ex, Object body, HttpHeaders headers,
			HttpStatus status, WebRequest request) {
		
		ErrorModelOut error = ErrorModelOut.builder()
			.statusCode(status.value())
			.message(GENERIC_ERROR_MESSAGE)
			.build();
		
		return super.handleExceptionInternal(ex, error, headers, status, request);
	}
}
