package com.chawico.product.entrypoint.model;

import java.util.Optional;

import com.chawico.product.entrypoint.model.in.ProductModelIn;
import com.chawico.product.entrypoint.model.out.ProductModelOut;
import com.chawico.product.usecase.domain.ProductDomain;

/**
 * Mapper para conversão entre objetos model e domain.
 * 
 * @author chawico
 * @since 12/04/2021
 */
public class ProductModelMapper {

	/**
	 * Construtor privado para evitar instanciação.
	 */
	private ProductModelMapper() {
		
	}
	
	public static ProductDomain toProductDomain(String id, ProductModelIn productRequest) {
		return ProductDomain.builder()
				.id(id)
				.name(productRequest.getName())
				.description(productRequest.getDescription())
				.price(productRequest.getPrice())
				.build();
	}
	
	public static ProductModelOut toModelOut(ProductDomain productDomain) {
		return ProductModelOut.builder()
				.id(productDomain.getId())
				.name(productDomain.getName())
				.description(productDomain.getDescription())
				.price(productDomain.getPrice())
				.build();
	}
	
	public static Optional<ProductModelOut> toModelOut(Optional<ProductDomain> productDomain) {
		ProductModelOut result = null;
		if (productDomain.isPresent()) {
			result = toModelOut(productDomain.get());
		}
		return Optional.ofNullable(result);
	}		
}
