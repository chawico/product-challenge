package com.chawico.product.entrypoint.model.in;

import java.beans.ConstructorProperties;
import java.math.BigDecimal;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;

/**
 * Representação dos dados de entrada REST de um Produto.
 * 
 * @author chawico
 * @since 12/04/2021
 */
@Getter
public class ProductModelIn {

	@ApiModelProperty(value = "Nome")
	@NotBlank
	private String name;
	
	@ApiModelProperty(value = "Descrição")
	@NotBlank
	private String description;
	
	@ApiModelProperty(value = "Preço")
	@NotNull
	@DecimalMin(value = "0.00", inclusive = false)
	private BigDecimal price;
	
	/**
	 * Construtor padrão.
	 * 
	 * @param name {@code String} - nome 
	 * @param description {@code String} - descrição
	 * @param price {@code BigDecimal} - preço
	 */
	@ConstructorProperties({"name", "description", "price"})
	public ProductModelIn(String name, String description, BigDecimal price) {
		this.name = name;
		this.description = description;
		this.price = price;
	}
}
