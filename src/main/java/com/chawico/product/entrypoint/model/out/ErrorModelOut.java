package com.chawico.product.entrypoint.model.out;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.Builder;
import lombok.Getter;

/**
 * Representação do modelo REST para retornos relacionados a erros.
 *  
 * @author chawico
 * @since 12/04/2021
 */
@Getter
@Builder
@JsonPropertyOrder({ "statusCode", "message" })
public class ErrorModelOut {

	@JsonProperty("status_code")
	private Integer statusCode;
	
	private String message;
}
