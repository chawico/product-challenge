package com.chawico.product.entrypoint.model.in;

import java.beans.ConstructorProperties;
import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;

/**
 * Representação dos dados de entrada REST para consultar um Produto.
 * 
 * @author chawico
 * @since 12/04/2021
 */
@Getter
public class ProductSearchModelIn {

	@JsonProperty("q")
	private String term;
	
	@JsonProperty("min_price")
	private BigDecimal minPrice;

	@JsonProperty("max_price")
	private BigDecimal maxPrice;
	
	@ConstructorProperties({"q", "min_price", "max_price"})
	public ProductSearchModelIn(String term, BigDecimal minPrice, BigDecimal maxPrice) {
		this.term = term;
		this.minPrice = minPrice;
		this.maxPrice = maxPrice;
	}	
}
