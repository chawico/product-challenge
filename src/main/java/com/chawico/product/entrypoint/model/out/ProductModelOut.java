package com.chawico.product.entrypoint.model.out;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Getter;

/**
 * Representação do modelo de saída REST de um Produto 
 * 
 * @author chawico
 * @since 12/04/2021
 */
@Getter
@Builder
@JsonPropertyOrder({ "id", "name", "description", "price" })
public class ProductModelOut {

	@ApiModelProperty(value = "Identificador")
	private String id;

	@ApiModelProperty(value = "Nome")
	private String name;

	@ApiModelProperty(value = "Descrição")
	private String description;
	
	@ApiModelProperty(value = "Preço")
	private BigDecimal price;
}
