package com.chawico.product.usecase;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.chawico.product.usecase.domain.ProductDomain;
import com.chawico.product.usecase.domain.ProductSearchDomain;
import com.chawico.product.usecase.gateway.ProductSearchGateway;

/**
 * Caso de uso referente a consulta avançada de Produtos. 
 * 
 * @author chawico
 * @since 12/04/2021
 */
@Service
public class ProductSearchUseCase {
	
	@Autowired
	private ProductSearchGateway productSearchGateway;

	/**
	 * Consulta todos os produtos cadastrados conforme critérios de busca.
	 *
	 * @param {@code ProductSearchDomain} - critérios de busca
	 * @return produtos encontrados
	 */	
	public List<ProductDomain> search(ProductSearchDomain productSearch) {
		return this.productSearchGateway.search(productSearch);
	}	
}
