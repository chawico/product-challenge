package com.chawico.product.usecase;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.chawico.product.usecase.domain.ProductDomain;
import com.chawico.product.usecase.gateway.ProductGateway;

/**
 * Caso de uso referente a cadastrar, alterar, remover ou consultar um Produto. 
 * 
 * @author chawico
 * @since 12/04/2021
 */
@Service
public class ProductUseCase {
	
	@Autowired
	private ProductGateway productGateway;

	/**
	 * Cria um produto. 
	 * 
	 * @param productDomain {@code ProductDomain} - dados de um produto
	 * @return produto cadastrado
	 */
	public ProductDomain criar(ProductDomain productDomain) {
		return this.productGateway.create(productDomain);
	}
	
	/**
	 * Atualiza um produto.
	 * 
	 * @param productDomain {@code ProductDomain} - dados de um produto
	 * @return produto atualizado ou vazio caso não exista o produto informado
	 */
	public Optional<ProductDomain> update(ProductDomain productDomain) {
		Optional<ProductDomain> existingProduct = this.findById(productDomain.getId());
		ProductDomain updatedProduct = null;
		if (existingProduct.isPresent()) {
			updatedProduct = this.productGateway.update(productDomain);
		}
		return Optional.ofNullable(updatedProduct);
	}

	/**
	 * Remove um produto específico.
	 * 
	 * @param id {@code String} - identificador do produto
	 * @return {@code true} caso o registro tenha sido encontrado e removido e {@code false} caso contrário
	 */
	public Boolean delete(String id) {
		Boolean deleted = Boolean.FALSE;
		
		if (this.findById(id).isPresent()) {
			this.productGateway.delete(id);
			deleted = Boolean.TRUE;
		}
		return deleted;
	}

	/**
	 * Consulta um produto específico.
	 * 
	 * @param id {@code String} - identificador do produto
	 * @return produto encontrado ou vazio caso não exista o produto requisitado
	 */
	public Optional<ProductDomain> findById(String id) {
		return this.productGateway.findById(id);
	}
	
	/**
	 * Consulta todos os produtos cadastrados.
	 * 
	 * @return produtos encontrados
	 */	
	public List<ProductDomain> findAll() {
		return this.productGateway.findAll();
	}
}
