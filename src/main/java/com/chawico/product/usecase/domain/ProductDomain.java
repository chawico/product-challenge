package com.chawico.product.usecase.domain;

import java.math.BigDecimal;

import lombok.Builder;
import lombok.Getter;

/**
 * Representação de um Produto no viés de negócio.
 * 
 * @author chawico
 * @since 12/04/2021
 */
@Getter
@Builder
public class ProductDomain {
	private String id;
	private String name;
	private String description;
	private BigDecimal price;
}
