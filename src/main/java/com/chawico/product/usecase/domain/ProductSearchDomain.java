package com.chawico.product.usecase.domain;

import java.math.BigDecimal;

import lombok.Builder;
import lombok.Getter;

/**
 * Representação do modelo de busca de Produtos no viés de negócio.
 * 
 * @author chawico
 * @since 12/04/2021
 */
@Getter
@Builder
public class ProductSearchDomain {

	private String term;
	private BigDecimal minPrice;
	private BigDecimal maxPrice;
}
