package com.chawico.product.usecase.gateway;

import java.util.List;

import com.chawico.product.usecase.domain.ProductDomain;
import com.chawico.product.usecase.domain.ProductSearchDomain;

/**
 * Gateway para realizar consultas avançadas de Produtos.
 * 
 * @author chawico
 * @since 12/04/2021
 */
public interface ProductSearchGateway {

	/**
	 * Consulta todos os produtos cadastrados conforme critérios de busca.
	 *
	 * @param {@code ProductSearchDomain} - critérios de busca
	 * @return produtos encontrados
	 */		
	List<ProductDomain> search(ProductSearchDomain productSearch);
}
