package com.chawico.product.usecase.gateway;

import java.util.List;
import java.util.Optional;

import com.chawico.product.usecase.domain.ProductDomain;

/**
 * Gateway para acessar dados referentes ao domínio Produto.
 * 
 * @author chawico
 * @since 12/04/2021
 */
public interface ProductGateway {

	/**
	 * Cria um produto. 
	 * 
	 * @param productDomain {@code ProductDomain} - dados de um produto
	 * @return produto cadastrado
	 */
	ProductDomain create(ProductDomain productDomain);

	/**
	 * Atualiza um produto.
	 * 
	 * @param productDomain {@code ProductDomain} - dados de um produto
	 * @return produto atualizado ou vazio caso não exista o produto informado
	 */
	ProductDomain update(ProductDomain productDomain);
	
	/**
	 * Remove um produto específico.
	 * 
	 * @param id {@code String} - identificador do produto
	 */	
	void delete(String id);
	
	/**
	 * Consulta um produto específico.
	 * 
	 * @param id {@code String} - identificador do produto
	 * @return produto encontrado ou vazio caso não exista o produto requisitado
	 */	
	Optional<ProductDomain> findById(String id);
	
	/**
	 * Consulta todos os produtos cadastrados.
	 * 
	 * @return produtos encontrados
	 */		
	List<ProductDomain> findAll();
}
