package com.chawico.product.configuration;

import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;

/**
 * Configura a resolução de mensagens.
 * 
 * @author chawico
 * @since 12/04/2021
 *
 */
@Configuration
public class MessageConfiguration {

	/**
	 * Bean para resolução de mensagens.
	 * 
	 * @return messageSource para resolução de mensagens
	 */
	@Bean
	public MessageSource messageSource() {
		ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
		messageSource.setBasename("classpath:messages");
		messageSource.setDefaultEncoding("UTF-8");
		
		return messageSource;
	}
}
