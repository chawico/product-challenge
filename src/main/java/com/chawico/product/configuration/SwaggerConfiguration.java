package com.chawico.product.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.RestController;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Configura a aplicação para ter o swagger para uso dos endpoints.
 *  
 * @author chawico
 * @since 12/04/2021
 */
@Configuration
@EnableSwagger2
public class SwaggerConfiguration {

	/**
	 * Bean para configuração do Swagger.
	 * 
	 * @return docket do swagger
	 */
    @Bean
    public Docket api() { 
        return new Docket(DocumentationType.SWAGGER_2)  
          .select()                                  
          .apis(RequestHandlerSelectors.withClassAnnotation(RestController.class))              
          .paths(PathSelectors.any())                          
          .build();                                           
    }	
}
