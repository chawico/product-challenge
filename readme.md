# O que é 
É uma proposta de solução ao problema descrito em https://bitbucket.org/RecrutamentoDesafios/desafio-java-springboot/src/master/ considerando o uso de Clean Architecture descrito em https://blog.cleancoder.com/uncle-bob/2012/08/13/the-clean-architecture.html

# Como executar?
Basta rodar o comando mvn spring-boot:run e a aplicação baixará as dependências e subirá a aplicação em http://localhost:9999